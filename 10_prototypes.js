// каждый объект имеет свой прототип, который берется от родителя
//
// __proto__ - прототип родителя (ES6)
// Object.getPrototypeOf() (ES5)
//
// цепочка прототипов

function Cat(name, color) {
  this.name = name
  this.color = color
}

Cat.prototype.voice = function(){
  console.log(`Cat ${this.name} says mya`)
}

const cat = new Cat('Musya','black')
console.log(Cat.prototype) // указатель на объект у которого есть конструктор и соответствующие
                           // поля и методы которые будут добавлены классу

console.log(cat) // не отображается voice, а он есть
console.log(cat.__proto__)
console.log(cat.__proto__ === Cat.prototype)
console.log(cat.constructor)
cat.voice()

// ============= собственные и унаследованные свойства
function Person(){}
Person.prototype.legs = 2
Person.prototype.skin = 'white'

const person = new Person()
person.name = 'Lev'

console.log('skin' in person)
console.log(person.legs)
console.log(person.eyes)
console.log(person.name)

// проверка наличие непосредственной свойства в объекте а не прототипе
console.log(person.hasOwnProperty('name'))
console.log(person.hasOwnProperty('skin'))

// Object.create()
let protoObj = {year: 2020}
const myYear = Object.create(protoObj)

console.log(myYear.year)
protoObj.year = 2222
console.log(myYear.year)

console.log(myYear.hasOwnProperty('year'))
// console.log(myYear.__proto__ === proto)
