// context - определяет как фукнция была вызвана
// и постоянно указывает на ключевое слово this
// call, apply, bind

const person = {
  surname: 'Stark',
  knows: function (what, name) {
    console.log(`You ${what} know, ${name} ${this.surname}`) // this указывает на объект person
  }
}

const john = {surname: 'Snow'}

person.knows('all', 'Bran')

// хотим вызвать knows() в контексте объекта john
person.knows.call(john, 'nothing', 'John') // первый параметр - переданный контекст
person.knows.apply(john, ['nothing', 'John'])
person.knows.call(john, ...['nothing', 'John'])

person.knows.bind(john, 'nothing', 'John')() // bind возвращает функцию с новым контестом
const newFunction = person.knows.bind(john, 'nothing', 'John')
newFunction()

// =======

function Person(name, age) {
  this.name = name
  this.age = age

  console.log(this)
}

const lev = new Person('Lev', 22)

// binding явный
function logThis() {
  console.log(this)
}

const obj = {num: 42}
logThis.apply(obj)
logThis.call(obj)
logThis.bind(obj)()

// binding неявный
const animal = {
  legs: 4,
  logThis: function () {
    console.log(this)
  }
}
animal.logThis()

// function создает себе новый контекст; arrow не создает контекст

function Cat(color) {
  this.color = color
  console.log('This', this);
  (() => console.log('Arrow this', this))()
}

new Cat('red')

function Cat2(color) {
  this.color = color
  console.log('This', this);
  function print(){console.log('Function this', this)}
  print()
}

new Cat2('red')
