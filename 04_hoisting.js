/*
  hoisting - всплытие.
  Т.е. например когда функцию можно вызвать до ее объявления

 */

console.log(sum(1,41))
function sum(a,b){
  return a + b
}
console.log(sum(1,41))

console.log(i)
var i = 42
console.log(i)

var z
console.log(z)
z = 42
console.log(z)

// console.log(num)
// const num = 42 // const и let не подвержены всплытию
// console.log(num)
//
// console.log(num2)
// let  num2 = 42 // const и let не подвержены всплытию
// console.log(num2)

// Function Expression & Function Declaration. Разные способы объявления функции
{
  // function declaration - всплывают
  console.log(square(5))
  function square(num){
    return num ** 2
  }

  // function expression - не всплывают
  console.log(square2(5))
  var square2 = function(num){
    return num ** 2
  }
}

