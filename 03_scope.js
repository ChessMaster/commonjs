/* scope - говорит нам о доступности переменных в нашем коде
   scope бывает локальный и глобальный

   глобальный - переменные объявлены вне функции,
   но доступны внутри всех функциях этого scope.

   локальный - переменная доступна в рамках одной функции или в дочерних

   scope указывает на доступность определенных переменных
   (область видимости)
*/

function funcA() {
  let a = 1

  function funcB() {
    let b = 2

    function funcC() {
      let c = 3

      console.log('funcC:', a, b, c)
    }

    funcC()
    console.log('funcB:', a, b)
  }

  funcB()
  console.log('funcA:', a)
}

funcA()
