// Immediate Invoked Function Expression
let result = []
for (var i = 0; i < 5; i++) {
  result.push(function () {
    console.log(i)
  })
}

result[2]()
result[4]()

let result2 = []
for (var i = 0; i < 5; i++) {
  (function(){
    var j = i
    result2.push(function(){
      console.log(j)})
  })()
}

result2[2]()
result2[4]()
