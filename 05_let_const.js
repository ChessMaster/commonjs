// let
let a = 'Variable a'
let b = 'Variable b'

{
  // block scope
}

{
  a = 'New Variable a'
  let b = 'Local Variable b'
  console.log('Scope A', a)
  console.log('Scope B', b)
}
console.log('A:',a)
console.log('B:',b)


// const
const PORT = 8080
  // PORT = 2000
const array = ['JavaScript', 'is', 'Awesome']
array.push('!')
array[0] = 'Java'
console.log(array)
  // array = ''

const obj = {}
obj.name = 'lev'
obj.age = 22
console.log(obj)
obj.age = 23
console.log(obj)
delete obj.name
console.log(obj)
