// js работает в одном потоке. Но асинхронность есть за счет EventLoop

const first = () => console.log('First')
const second = () => console.log('Second')
const third = () => console.log('Third')

// без неожиданностей
first()
second()
third()

//
first() // отправляется в call stack. Выполняется. Выбрасывается из стека

setTimeout(second,0) // стек. Запрос на стороннее api. Выход из стека

third() // отправляется в call stack. Выполняется. Выбрасывается из стека

/*
 после завершения setTimeout функция second помечается, как необходимая к выполнению
 и отправляется в callback queue.
 Когда call stack пуст - выполняются функции из callback queue
 */
