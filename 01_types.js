// null, undefined, boolean, number, string, object, symbol

console.log(typeof 0)
console.log(typeof true)
console.log(typeof 'Javascripts') // "", '', `${dynamicData}`
console.log(typeof undefined) // не объявлена или не присвоено значение
console.log(typeof {})
console.log(typeof Symbol('JS'))

console.log(typeof null) // Неточность typeof / объявлена, но нет значения("no value")
console.log(typeof function(){}) // Неточность typeof

console.log(typeof NaN)
console.log(typeof (1/0))

// Приведение типов
let language = 'JavaScript'
if(language){
  console.log('The best language is: ',language)
}
  // falsy values: '', 0, null, undefined, NaN, false
console.log(Boolean(''))
console.log(Boolean(0))
console.log(Boolean(null))
console.log(Boolean(undefined))
console.log(Boolean(NaN))
console.log(Boolean(false))

console.log(Boolean([])) // true
console.log(Boolean({})) // true
console.log(Boolean(function(){})) // true

  // Строки и числа
console.log(1 + '2') // string 12
console.log('' + 1 + 0) // string 10
console.log('' - 1 + 0) // number -1
console.log('3' * '8') // number 24
console.log(4 + 10 + 'px') // Порядок важен: string 14px
console.log('px' + 5 + 10) // Порядок важен: string px510
console.log('42' - 40) // number 40
console.log('42px' - 2) // number NaN
console.log(null + 2) // number 2
console.log(undefined + 42) // undefined, так как он не приводится к числу

  // == vs === : == - сначала приведение типов; === - без приведения типов.
  // рекомендуется всегда использовать ===
console.log((2 == '2')) // true
console.log((2 === '2')) // false
console.log(undefined == null) // true
console.log(undefined === null) // false
console.log('0' == false)
console.log('0' == 0)
console.log(0 == 0)

// ===== неочевидные примеры
console.log(false == '')
console.log(false == [])
console.log(false == {})

console.log('' == 0)
console.log('' == [])
console.log('' == {})

console.log(0 == [])
console.log(0 == {})
console.log(0 == null)
