{
  let a = 32
  let b = a
  b++

  console.log('a', a)
  console.log('b', b)
}

{
  let a = [1, 2, 3]
  let b = a
  a.push(4)

  console.log('a', a)
  console.log('b', b)
}

{
  let a = [1, 2, 3]
  let b = a.concat() // возвращает копию массива
  a.push(4)

  console.log('a', a)
  console.log('b', b)
  console.log(a === b)
}

{
  let a = [1, 2, 3]
  let b = a
  a.push(4)

  let c = [1,2,3,4]

  console.log(a === b)
  console.log(a === c)
}

